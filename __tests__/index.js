const { gql } = require("apollo-server");
const { createTestClient } = require("apollo-server-testing");
const serverTest = require("../index");
const { getPayload } = require('../helpers/util');

describe('Users', () => {
    //this test case will generate a successfull authentication, using the right credentials.
    it('should authenticate user', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const username = 'user1@sideinc.com';
            const password = '676cfd34-e706-4cce-87ca-97f947c43bd4';
            const authorization = '';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { username, password, loggedIn };
        };

        const response = await query({
            query: gql`
            query Query {
                login
                }
                
            ` });
        const token = response && response.data ? response.data.login : null
        expect(token).not.toBeNull();
    });
    //this test case will generate a fail authentication, using the wrong credentials.
    it('should not authenticate user with wrong user and password', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const username = 'wrongUser';
            const password = 'wrongPassword';
            const authorization = '';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { username, password, loggedIn };
        };

        const response = await query({
            query: gql`
            query Query {
                login
                }
                
            ` });
        const error = response.errors && response.errors[0] ? response.errors[0].message : null
        expect(error).toBe('Wrong Credentials!');
    });
    //this test case will generate a fail authentication, using blank credentials.
    it('should not authenticate user with blank user and password', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const username = '';
            const password = '';
            const authorization = '';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { username, password, loggedIn };
        };

        const response = await query({
            query: gql`
            query Query {
                login
                }
                
            ` });
        const error = response.errors && response.errors[0] ? response.errors[0].message : null
        expect(error).toBe('Wrong Credentials!');
    });
});


describe('Properties', () => {
    //this test case will generate a successfull getProperties, using the right credentials and a valid token generated from this credentials.
    it('should retrieve properties', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const username = 'user1@sideinc.com';
            const password = '676cfd34-e706-4cce-87ca-97f947c43bd4';
            const authorization = '';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { username, password, loggedIn };
        };

        const responseLogin = await query({
            query: gql`
            query Query {
                login
                }
            ` });
        const token = responseLogin && responseLogin.data ? responseLogin.data.login : null
        serverTest.context = () => {
            const authorization = 'Bearer '+token;
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { loggedIn };
        };
        const responseProperties = await query({
            query: gql`
            query Query {
                getProperties (city: "Oak Ridge"){ 
                  mlsId
                        address {
                          city,
                          country,
                          full
                        }
                 }
                }                
            ` });
        expect(responseProperties.data).not.toBeNull();
    });
    //this test case will generate a fail getProperties, using a invalid token on authorization header.
    it('should not retrieve properties with wrong token', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const authorization = 'Bearer wrongToken';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { loggedIn };
        };
        const responseProperties = await query({
            query: gql`
            query Query {
                getProperties (city: "Oak Ridge"){ 
                  mlsId
                        address {
                          city,
                          country,
                          full
                        }
                 }
                }                
            ` });
        const error = responseProperties.errors && responseProperties.errors[0] ? responseProperties.errors[0].message : null
        expect(error).toBe('Token not valid!');
    });
    //this test case will generate a fail getProperties, using a blank token on authorization header.
    it('should not retrieve properties with blank token', async () => {
        const { query } = createTestClient(serverTest);
        serverTest.context = () => {
            const authorization = '';
            let loggedIn = false;
            if (authorization) {
                const [type, token] = authorization ? authorization.split(' ') : null;
                const response = getPayload(token);
                loggedIn = response.loggedIn
            }
            return { loggedIn };
        };
        const responseProperties = await query({
            query: gql`
            query Query {
                getProperties (city: "Oak Ridge"){ 
                  mlsId
                        address {
                          city,
                          country,
                          full
                        }
                 }
                }                
            ` });
        const error = responseProperties.errors && responseProperties.errors[0] ? responseProperties.errors[0].message : null
        expect(error).toBe('Token not valid!');
    });
});