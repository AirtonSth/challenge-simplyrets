const axios = require('axios');
const { getToken, verifyCredentials } = require("../helpers/util")
const { AuthenticationError, ApolloError } = require('apollo-server');
const config = require('../config');

const resolvers = {
  Query: {
    //this method retrieve the properties from api
    getProperties: async (parent, { city }, context, info) => {
      try {
        //it's a method with authorization required, so will throw a new error in case of a user not logged in.
        if (!context.loggedIn) {
          throw new AuthenticationError("Token not valid!")
        }
        //in case of a user successfull logged in, it will consume the api from simplyrets to get the properties.
        //Also, in case of receive a city param from request, it will be used to get the properties to this specific city.
        const response = await axios.get('https://api.simplyrets.com/properties'+(city ? `?q=${city}` : ''), {
          auth: {
            username: config.apiAuthentication.username,
            password: config.apiAuthentication.password
          }
        });
        return response.data;
      } catch (error) {
        //in case of some problem related to authorization or to api request, it will go throw a new error.
        throw new ApolloError(error.message ? error.message : "Error on trying to get properties.")
      }
    
    },
    //this method allow the user to login
    login: async (parent, args, context, info) => {
      //here it will verify if the credentials from request are valid.
      const isMatch = await verifyCredentials(context.username, context.password)
      if (isMatch) {
          //in case of valid credentials, it will generate a new JWT token to return with the response.
          const token = getToken()
          return token;
      } else {
          throw new AuthenticationError("Wrong Credentials!")
      }
  },
  },
};

module.exports = resolvers