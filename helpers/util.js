const jwt = require("jsonwebtoken");
const config = require('../config');

//this method will verify if the credentials are correct.
const verifyCredentials = (user, password) => new Promise(async (resolve, reject) => {
	if(user==config.appAuthentication.username && password==config.appAuthentication.password){
        //if it's correct, will resolve as true;
		resolve(true)
	}else{
        //if not, will resolve as false;
		resolve(false)
	}
	
})
//this method will generated a JWT Token, that expires in 1 week, based on the credentials.
const getToken = () => {
    const token = jwt.sign({username : config.appAuthentication.username, password : config.appAuthentication.password}, config.secret, {
        expiresIn: 604800, // 1 Week
    })
    return token
}
//this method will verify if the informed token is right.
const getPayload = token => {
    try {
        //it will verify the token comparing with the secret. In case of true, will return the loggedIn equals to true and also the payload.
        const payload = jwt.verify(token, config.secret);
        return { loggedIn: true, payload };
    } catch (err) {
        // In case of false, will return the loggedIn equals to false.
        return { loggedIn: false }
    }
}

module.exports = {
    getToken,
    getPayload,
    verifyCredentials
}
