** How to Use **
1 - you need to request login method, and send on the requests headers 2 params. A "username" key with value "user1@sideinc.com" and a "password" key with value "676cfd34-e706-4cce-87ca-97f947c43bd4". It will return a JWT token, which you will use to authenticate on getProperties method.
    login query example:
        query Query {
            login
        }
2 - you will request getProperties method, and send on the request header the Jwt token, with this format: A "Authorization" key with value "Bearer MyJwtToken". You can also send a city param to filter the api, or with you dont want to filter just don't send the city param.
    getProperties query example with city param:
        query Query {
                getProperties (city: "Oak Ridge"){ 
                        mlsId
                        address {
                        city,
                        country,
                        full
                        }
                }
        }
    getProperties query example without city param:
        query Query {
                getProperties{ 
                        mlsId
                        address {
                        city,
                        country,
                        full
                        }
                }
        }


** Folders and Files Structure **
-- __tests__ (folder with tests files)
             -- index.js
-- config (folder with a config file, to help with some important variables such as passwords, secrets, usernames)
          -- index.js
-- helpers (folder with a util file, to help with some security methods.)
          -- util.js
-- resolvers (folder with resolver file)
             -- resolvers.js
-- types (folder with index file containing the types definition)
         -- index.js
-- index.js (main file of server, which is used to create the AppoloServer, receive some headers and verify if the user is logged in, in case of authorization header)

** Important Notes **
- This project is using jest to execute the tests, where we have 6 test cases coveraging all the code.
- To integrate with api it is using axios package.
- It have a config file with some variables such as right credentials, api credentials, jwt secret, and this file is used in some another places such as util.js, and resolvers.js.
- it have a util.js file that help with some security methods to verify credentials, generate JTW token, verify JWT token.
- on resolvers.js it have 2 methods, getProperties and login. The login will receive username and password and verify if this credentials is valid, and return (in case of valid credentials) a JWT token in the response. The getProperties will integrate with API to retrieve the properties. But to use this method you need to provided a valid JWT Token. And this method accept a ciy param, which can be use to filter the properties API.
